import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import { changeFirstName as _changeFirstName } from './actions/userActions';

class App extends Component {

  changeFirstName = (e) => {
    this.props.changeFirstName(e.target.value);
  };

  render() {
    return (
      <div className="App">
        <input value={this.props.user.firstName} onChange={this.changeFirstName} />

        <pre style={{textAlign: "left"}}>
         {
           JSON.stringify(this.props, null, 2)
         }
        </pre>

      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  changeFirstName: (value) => dispatch(_changeFirstName(value))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
