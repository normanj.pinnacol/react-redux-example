export const changeFirstName = (value) => (dispatch) => {
  dispatch({
    type: 'CHANGE_FIRST_NAME',
    payload: value,
  })
};
