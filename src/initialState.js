const initialState = {
  user: {
    firstName: 'bob',
    lastName: 'smith',
    age: 42,
    occupation: 'sales',
  },
  items: [
    {
      id: 111,
      title: 'some awesome thing',
      quantity: 3,
    },
    {
      id: 222,
      title: 'some less awesome thing',
      quantity: 1,
    },
    {
      id: 333,
      title: 'the thing no one wants',
      quantity: 17,
    },
  ]
};

export default initialState;
