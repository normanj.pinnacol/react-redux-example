import initialState from '../initialState';

export default (state = initialState.items, action) => {
  switch (action.type) {
    case 'ADD_ITEM':
      return {
        result: action.payload
      };
    default:
      return state
  }
}
