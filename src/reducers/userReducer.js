import initialState from '../initialState';

export default (state = initialState.user, action) => {
  switch (action.type) {
    case 'CHANGE_FIRST_NAME':
      return Object.assign({}, state, { firstName: action.payload });
    default:
      return state
  }
}
